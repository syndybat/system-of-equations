// soustava_rovnic.cpp : Defines the entry point for the console application.

#include "stdafx.h"
#include "matic.h"
#include <stdlib.h>
#include <stdio.h>

char rovnice[100];
double**maticeA;
double*	maticeC;

char prom[]={"xyzabcdefghijklmnopqrstuv"};
bool setting[4]={true,true,false,false};
char cesta[30];

unsigned short int n=2,k=0; //k+1 rovnic n=pocet prom

int _tmain(int argc, _TCHAR* argv[])
{
	headlline();
	
	//uvod
	printf("defaultni nastaveni: mezivysledky-ano\n");
	printf("                     vypis v consoli-ano \n");
	printf("                     vypis v souboru-ne\n");
	printf("                     nacteni ze soubotu-ne\n\n");
	printf("chcete zmenit nastaveni?: ");
	fflush(stdin);
	if (getchar()=='y') set(setting);
	fflush(stdin);
	system("cls");

	do//zacyklen�
	{
	headlline();


		if (setting[3])
		{

			//cteni ze souboru
			printf_s("	Zadejte nazev souboru vcetne pripony"".txt"":\n\n");
			gets_s(cesta,30);
			FILE* Csoubor;
			if ((Csoubor = fopen(cesta, "r")) == NULL) 
				{
				printf("soubor nelze otevrit!\n");
				printf("chcete to zkusit znovu?: ");
				fflush(stdin);
				if(getchar()=='y')
					{
					printf("chcete zmenit nestaveni?");
					fflush(stdin);
					if(getchar()=='y')
						{
						set(setting);
						}
					fflush(stdin);
					system("cls");
					continue;			
					}
				exit(1);
				}

			fscanf(Csoubor,"%[^\n]\n",rovnice);
			n=typ_M(rovnice);			
			init(n,&maticeA,&maticeC);
			trim(maticeA,maticeC, rovnice,k);
			k++;
	
			//nacitani rovnic
			while (k<n)
				{
				fscanf(Csoubor,"%[^\n]\n",rovnice);
				trim(maticeA,maticeC, rovnice,k);
				k++;	
				}
			fclose(Csoubor);
			}
		else
		{
		//cteni z konzole
		printf_s("	Zadejte soustavu:\n\n");
		gets_s(rovnice,100);
		n=typ_M(rovnice);			
		init(n,&maticeA,&maticeC);
		trim(maticeA,maticeC, rovnice,k);
		k++;
	
		//nacitani rovnic
		while (k<n)
			{
			gets_s(rovnice,100);
			trim(maticeA,maticeC, rovnice,k);
			k++;	
			}
		}

	
	system("cls");
	headlline();
	printf_s("	resena soustava:\n");
	Mprint(n,maticeA,maticeC,prom,setting);
	
	//pocitani
	if (ready (n,maticeA,maticeC))
		{
			if(setting[0])//podminka tisku mezi vypoctu
			{
			printf_s("	!!!doslo k prerovnani rovnic!!!\n");
			Mprint(n,maticeA,maticeC,prom,setting);
			}
		}

	
	steady (n,maticeA,maticeC);
	if(setting[0])//podminka tisku mezi vypoctu
			{
			printf_s("	po gausove uprave:\n");
			Mprint(n,maticeA,maticeC,prom,setting);
			}
	
	printf_s("		reseni:\n");
	if(solve (n,maticeA,maticeC,prom))
		ResultPrint(n,maticeA,maticeC,prom,setting);
	
	
	
	fflush(stdin);
	printf_s("chcete vypocitat novou rovnici?:");
	if (getchar()!='y')
		break;
	fflush(stdin);
	printf_s("chcete zmenit nastaveni?:");
	if (getchar()=='y')
		{
			fflush(stdin);
			set(setting);
		}
	fflush(stdin);
	uninit(n,&maticeA,&maticeC);
	k=0;
	system("cls");
	}while(true);
	// ukonceni
	uninit(n,&maticeA,&maticeC);
	
	return 0;
}

