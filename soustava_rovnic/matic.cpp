#include "stdafx.h"
#include "matic.h"
#include <stdlib.h>
#include <string.h>

//titulek programu
void headlline()
{
	printf_s("************************************************************\n");
	printf_s("***Toto je program pro vypocet soustavy linearnich rovnic***\n");
	printf_s("***      zapisujete ve tvaru a b c d ... n = z           ***\n");
	printf_s("***      na otazky odpovidejte (y/n) y-ano, n-ne         ***\n");
	printf_s("************************************************************\n\n");
} 
//nastaveni programu
void set(bool setting[4])
{
	char a;
	
	fflush(stdin);
	printf_s("chcete tisknout mezi vypocty?:");
	scanf_s("%c",&a);

	(a=='y') ? setting[0]=true : setting[0]=false ;
	fflush(stdin);
	
	printf_s("chcete tisknout do console?:");
	scanf_s("%c",&a);
	(a=='y') ? setting[1]=true : setting[1]=false ;
	fflush(stdin);

	printf_s("chcete tisknout do souboru?:");
	scanf_s("%c",&a);
	(a=='y') ? setting[2]=true : setting[2]=false ;
	fflush(stdin);

	printf_s("chcete nacist soustavu ze souboru?:");
	scanf_s("%c",&a);
	(a=='y') ? setting[3]=true : setting[3]=false ;
	fflush(stdin);

	return;
}
//zjisti velikost
int typ_M (char rovnice[100]) //vypocita velikost matice
{
	unsigned short int i=0,pocet=0;
	while (rovnice[i]!='=')
	{
		if (rovnice[i]==' ')
			pocet++;
		i++;
	}
	return pocet;
}
//alokuje pam�t 
void init(unsigned short int n,double***maticeA,double**maticeC)
{
        double **matice = (double **) malloc(n*sizeof(double *));
        int i=0,j=0;
        for(i=0;i<n;i++)
        {
                matice[i]= (double *) malloc(n*sizeof(double));
                for (int j=0;j<n;j++)
                        matice[i][j]=0;
        }
		*maticeA=matice;

//maticeC
        double *vektor = (double *) malloc(n*sizeof(double));
        for(i=0;i<n;i++)
                vektor[i]=0;
        *maticeC=vektor;
}
//rozt��d� hodnoty do matic
void trim (double** maticeA,double* maticeC, char rovnice[100],unsigned short int k) // rozseka rovnici do matice // k=cislo rovnice
{
	int i=0,j=0,l=0;
	char cislo[15]="";
	
	do
	{
		if (rovnice[i]!=' ')
		{
			cislo[j] = rovnice[i];
			j++;
			i++;
			continue;
		}

		maticeA[k][l]=atof(cislo);
		i++;
		l++;
		memset(cislo, 'a', sizeof(char)*14);
		j=0;
		
	}while (rovnice[i]!='=');
	
	i++;
	while (rovnice[i]!='\0')
	{
		if (rovnice[i]!=' ')
		{
			cislo[j] = rovnice[i];
			j++;
			i++;
			continue;
		}
		i++;
	}
	maticeC[k]=atof(cislo);
return;
}
//proh�z� ��dky aby na diagon�le nebyla 0
bool ready (unsigned short int n,double** maticeA,double* maticeC) //pripravy rovnici aby se na hlavni diagonale nenachazeli nuly
{
	unsigned short int i,j;
	double* temp=NULL;
	double docas=0;
	bool prohod=false;
	
	for(i=0;i<n;i++)
	{
		if(maticeA[i][i]==0)
		{
			for(j=1;i+j<n;j++)
			{
				if(maticeA[i+j][i]!=0)
				{
					temp=maticeA[i+j];
					maticeA[i+j]=maticeA[i];
					maticeA[i]=temp;

					docas=maticeC[i+j];
					maticeC[i+j]=maticeC[i];
					maticeC[i]=docas;
					
					prohod=true;
					break;
				}
			}
		}
	}
return prohod;
}
//uprav� na gausovou elimina�n� metodou na troj�heln�k
void steady (unsigned short int n,double** maticeA,double* maticeC)
{//nuluje hodnoty pod hlavni diagonalou
	double nasobitel=0;
       int i=0,j=0, k=0; //i-kterou kratim ostatni j-sloupec s kterym pracuji, k - ktery je nulovan
        for(i=0;i<(n-1);i++)
        {    
		 for (k=0;(i+k+1)<n;k++)
			 {  
				if(maticeA[k+i+1][i]==0) continue;
				nasobitel=((maticeA[k+i+1][i])/(maticeA[i][i]));
				for (j=0;(i+j)<n;j++)
					{
					maticeA[k+i+1][i+j]-=((maticeA[i][i+j])*nasobitel);
					}
				maticeC[k+i+1]-=maticeC[i]*nasobitel;
			 }

        }
	//ziskava 1 na hlavni diagonale
	double delitel=0;
	 for(i=0;i<n;i++)
		{
			if (maticeA[i][i]==0) continue;
			delitel	= maticeA[i][i];
			for (j=0;(i+j)<n;j++)
				{
				maticeA[i][i+j]/=delitel;
				}
		maticeC[i]/=delitel;		
        }	
	return;	
}
//uprav� rovnice aby na diagon�le byla hodnota 1 a dosazov�n�m uprav� na jednotkou matici
bool solve (unsigned short int n,double** maticeA,double* maticeC, char prom[])
{
	//tvori jednotkovou matici dosazovanim

	 int i=0,j=0;
	 for(i=(n-2);i>=0;i--)
		{
			
			for (j=(n-1);(j)>i;j--)
				{
				maticeC[i]-=maticeA[i][j]*maticeC[j];
				maticeA[i][j]=0;
				}
			
        }
	 //kontroluje resitelnost soustavy
	for(i=0;i<n;i++)
	  {
			if ((maticeA[i][i]==0)&&(maticeC[i]==0))
			{
			printf_s("soustava ma nekonecne mnoho reseni\n");
			return false;
			}
			if((maticeA[i][i]==0)&&(maticeC[i]!=0))
			{
			printf_s("soustava nema reseni\n");
			return false;
			}
		
	  }


	 return true;
}
// tiskne matici
void Mprint(unsigned short int n,double **maticeA,double *maticeC,char prom[],bool setting[3])
	{
	unsigned short int i=0,j=0;
	// console	
	if (setting[1])
	{
	for (i=0;i<n;i++)
		{
			for (j=0;j<n;j++)
			{
				printf_s("%6.2lf%c ",maticeA[i][j],prom[j]);
			}
			printf_s("= %4.2lf \n",maticeC[i]);
		}
	printf_s("\n\n");
	}
	//soubor
	if (setting[2])
	{
	FILE *soubor;
	soubor = fopen("vysledky.txt", "a");
	fprintf_s(soubor,"\n\n");
	for (i=0;i<n;i++)
		{
			for (j=0;j<n;j++)
			{
				fprintf_s(soubor,"%6.2lf%c ",maticeA[i][j],prom[j]);
			}
			fprintf_s(soubor,"= %4.2lf \n",maticeC[i]);
		}
	fprintf_s(soubor,"\n\n");
	fclose(soubor);
	}

	return;
	}
// vytiskne vysledky
void ResultPrint(unsigned short int n,double **maticeA,double *maticeC,char prom[],bool setting[3])
{
	unsigned short int i=0;
	// console	
	if (setting[1])
	{
	 printf_s("		***************************\n");
	 for(i=0;i<n;i++)
		{
		printf_s("		***	%c = %6.2lf	***\n",prom[i],maticeC[i]);
        }
	 printf_s("		***************************\n");
	}
	//soubor
	if (setting[2])
	{
	FILE *soubor;
	soubor = fopen("vysledky.txt", "a");
	fprintf_s(soubor,"\n\n");
	fprintf_s(soubor,"		***************************\n");
	for (i=0;i<n;i++)
		{	
		fprintf_s(soubor,"		***	%c = %6.2lf	***\n",prom[i],maticeC[i]);
		}
	fprintf_s(soubor,"\n*************************************************************************\n\n");
	fclose(soubor);
	}

	return;
}
// uvoln� pam�t
void uninit(unsigned short int n,double***maticeA,double**maticeC)
	{
	//maticeA
	int i;
	for(i=0;i<n;i++)
	{
		free((*maticeA)[i]);
	}
	free(*maticeA);

//maticeC
	free(*maticeC);	
	return;
	}

