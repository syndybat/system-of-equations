#include "stdafx.h"


void headlline();
void set(bool setting[4]);
int typ_M (char rovnice[100]);//velikost matice
void init(unsigned short int n,double***maticeA,double**maticeC);//dynamicka initzializace
void trim (double**maticeA,double*maticeC, char rovnice[],unsigned short int k);//rozsekani rovnice do matice
bool ready (unsigned short int n,double** maticeA,double* maticeC);
void steady (unsigned short int n,double** maticeA,double* maticeC); // uprava gauss
bool solve (unsigned short int n,double** maticeA,double* maticeC,char prom[]);
void Mprint(unsigned short int n,double**maticeA,double*maticeC,char prom[],bool setting[4]);
void ResultPrint(unsigned short int n,double **maticeA,double *maticeC,char prom[],bool setting[3]);
void uninit(unsigned short int n,double***maticeA,double**maticeC);//dynamicka uninitzializace

